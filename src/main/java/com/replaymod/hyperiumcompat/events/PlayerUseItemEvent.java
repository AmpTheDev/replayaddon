package com.replaymod.hyperiumcompat.events;

import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;

@RequiredArgsConstructor
public class PlayerUseItemEvent {
    public final EntityPlayer entityPlayer;
}
