package com.replaymod.hyperiumcompat.events.mixins;

import com.replaymod.hyperiumcompat.ReflectionHelper;
import com.replaymod.hyperiumcompat.events.PlayerSleepInBedEvent;
import com.replaymod.hyperiumcompat.events.PlayerUseItemEvent;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatBase;
import net.minecraft.stats.StatList;
import net.minecraft.util.BlockPos;
import net.minecraft.util.FoodStats;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityPlayer.class)
public abstract class MixinEntityPlayer extends EntityLivingBase {
    public MixinEntityPlayer(World worldIn) {
        super(worldIn);
    }

    @Shadow public abstract boolean isSpectator();

    @Shadow private ItemStack itemInUse;

    @Shadow public InventoryPlayer inventory;

    @Shadow private int itemInUseCount;

    @Shadow protected abstract void updateItemUse(ItemStack itemStackIn, int p_71010_2_);

    @Shadow public abstract void clearItemInUse();

    @Shadow public int xpCooldown;

    @Shadow private int sleepTimer;

    @Shadow protected abstract boolean isInBed();

    @Shadow public abstract void wakeUpPlayer(boolean p_70999_1_, boolean updateWorldFlag, boolean setSpawn);

    @Shadow public Container openContainer;

    @Shadow protected abstract void closeScreen();

    @Shadow public Container inventoryContainer;

    @Shadow public PlayerCapabilities capabilities;

    @Shadow public double prevChasingPosX;

    @Shadow public double chasingPosX;

    @Shadow public double chasingPosY;

    @Shadow public double chasingPosZ;

    @Shadow public double prevChasingPosY;

    @Shadow public double prevChasingPosZ;

    @Shadow private BlockPos startMinecartRidingCoordinate;

    @Shadow protected FoodStats foodStats;

    @Shadow public abstract void triggerAchievement(StatBase achievementIn);

    @Inject(method = "trySleep", at = @At("HEAD"))
    public void trySleep(BlockPos bedLocation, CallbackInfoReturnable ci) {
        cc.hyperium.event.EventBus.INSTANCE.post(new PlayerSleepInBedEvent((EntityPlayer) (Object) this, bedLocation));
    }
    @Inject(method = "setItemInUse", at = @At("HEAD"))
    public void setItemInUse(ItemStack stack, int duration, CallbackInfo ci) {
        cc.hyperium.event.EventBus.INSTANCE.post(new PlayerUseItemEvent((EntityPlayer) (Object) this));
    }
    /**
     * @reason events
     * @author amp
     */
    @Overwrite
    public void onUpdate() {
        this.noClip = this.isSpectator();

        if (this.isSpectator())
        {
            this.onGround = false;
        }

        if (this.itemInUse != null)
        {
            ItemStack itemstack = this.inventory.getCurrentItem();

            if (itemstack == this.itemInUse)
            {
                cc.hyperium.event.EventBus.INSTANCE.post(new PlayerUseItemEvent((EntityPlayer) (Object) this));
                if (this.itemInUseCount <= 25 && this.itemInUseCount % 4 == 0)
                {
                    this.updateItemUse(itemstack, 5);
                }

                if (--this.itemInUseCount == 0 && !this.worldObj.isRemote)
                {
                    ReflectionHelper.getMethod(this, "onItemUseFinish", "s").invoke();
                }
            }
            else
            {
                this.clearItemInUse();
            }
        }

        if (this.xpCooldown > 0)
        {
            --this.xpCooldown;
        }

        if (this.isPlayerSleeping())
        {
            ++this.sleepTimer;

            if (this.sleepTimer > 100)
            {
                this.sleepTimer = 100;
            }

            if (!this.worldObj.isRemote)
            {
                if (!this.isInBed())
                {
                    this.wakeUpPlayer(true, true, false);
                }
                else if (this.worldObj.isDaytime())
                {
                    this.wakeUpPlayer(false, true, true);
                }
            }
        }
        else if (this.sleepTimer > 0)
        {
            ++this.sleepTimer;

            if (this.sleepTimer >= 110)
            {
                this.sleepTimer = 0;
            }
        }

        super.onUpdate();

        if (!this.worldObj.isRemote && this.openContainer != null && !this.openContainer.canInteractWith((EntityPlayer) (Object) this))
        {
            this.closeScreen();
            this.openContainer = this.inventoryContainer;
        }

        if (this.isBurning() && this.capabilities.disableDamage)
        {
            this.extinguish();
        }

        this.prevChasingPosX = this.chasingPosX;
        this.prevChasingPosY = this.chasingPosY;
        this.prevChasingPosZ = this.chasingPosZ;
        double d5 = this.posX - this.chasingPosX;
        double d0 = this.posY - this.chasingPosY;
        double d1 = this.posZ - this.chasingPosZ;
        double d2 = 10.0D;

        if (d5 > d2)
        {
            this.prevChasingPosX = this.chasingPosX = this.posX;
        }

        if (d1 > d2)
        {
            this.prevChasingPosZ = this.chasingPosZ = this.posZ;
        }

        if (d0 > d2)
        {
            this.prevChasingPosY = this.chasingPosY = this.posY;
        }

        if (d5 < -d2)
        {
            this.prevChasingPosX = this.chasingPosX = this.posX;
        }

        if (d1 < -d2)
        {
            this.prevChasingPosZ = this.chasingPosZ = this.posZ;
        }

        if (d0 < -d2)
        {
            this.prevChasingPosY = this.chasingPosY = this.posY;
        }

        this.chasingPosX += d5 * 0.25D;
        this.chasingPosZ += d1 * 0.25D;
        this.chasingPosY += d0 * 0.25D;

        if (this.ridingEntity == null)
        {
            this.startMinecartRidingCoordinate = null;
        }

        if (!this.worldObj.isRemote)
        {
            this.foodStats.onUpdate((EntityPlayer) (Object) this);
            this.triggerAchievement(StatList.minutesPlayedStat);

            if (this.isEntityAlive())
            {
                this.triggerAchievement(StatList.timeSinceDeathStat);
            }
        }

        int i = 29999999;
        double d3 = MathHelper.clamp_double(this.posX, -2.9999999E7D, 2.9999999E7D);
        double d4 = MathHelper.clamp_double(this.posZ, -2.9999999E7D, 2.9999999E7D);

        if (d3 != this.posX || d4 != this.posZ)
        {
            this.setPosition(d3, this.posY, d4);
        }
    }
    @Inject(method = "stopUsingItem", at = @At("HEAD"))
    public void stopUsingItem(CallbackInfo ci) {
        cc.hyperium.event.EventBus.INSTANCE.post(new PlayerUseItemEvent((EntityPlayer) (Object) this));
    }
    @Inject(method = "onItemUseFinish", at = @At("HEAD"))
    public void onItemUseFinish(CallbackInfo ci) {
        cc.hyperium.event.EventBus.INSTANCE.post(new PlayerUseItemEvent((EntityPlayer) (Object) this));
    }
}
