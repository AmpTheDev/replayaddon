package com.replaymod.hyperiumcompat.events.mixins;

import com.replaymod.hyperiumcompat.events.MinecartInteractEvent;
import net.minecraft.entity.item.EntityMinecartContainer;
import net.minecraft.entity.item.EntityMinecartEmpty;
import net.minecraft.entity.player.EntityPlayer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityMinecartEmpty.class)
public class MixinMinecartEmpty {
    @Inject(method = "interactFirst", at = @At("HEAD"))
    public void interactFirst(EntityPlayer player, CallbackInfoReturnable ci) {
        cc.hyperium.event.EventBus.INSTANCE.post(new MinecartInteractEvent(player, (EntityMinecartContainer) (Object) this));
    }
}
