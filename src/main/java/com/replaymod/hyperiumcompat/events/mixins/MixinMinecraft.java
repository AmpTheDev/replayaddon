package com.replaymod.hyperiumcompat.events.mixins;

import com.replaymod.core.utils.ReplayModPack;
import com.replaymod.hyperiumcompat.ReflectionHelper;
import java.io.File;
import java.util.List;
import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.DefaultResourcePack;
import net.minecraft.client.resources.IResourcePack;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Minecraft.class)
public class MixinMinecraft {

  @Shadow @Final private List<IResourcePack> defaultResourcePacks;

  @Shadow @Final private DefaultResourcePack mcDefaultResourcePack;

  @Inject(method = "startGame", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/Minecraft;startTimerHackThread()V"))
  public void onConstruct(CallbackInfo ci) {
    System.out.println("[replaymod] oof lol");
    defaultResourcePacks.add(new ReplayModPack((Map<String, File>) ReflectionHelper.getField(mcDefaultResourcePack, "mapAssets", "b").get()));
  }
}
