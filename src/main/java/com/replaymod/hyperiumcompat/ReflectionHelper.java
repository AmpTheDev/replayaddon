package com.replaymod.hyperiumcompat;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Timer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionHelper {
    public static FieldWrapper getField(Class<?> clazz, String deobfName, String obfName) {
        try {
            Field field = clazz.getDeclaredField(deobfName);
            field.setAccessible(true);
            return new FieldWrapper(field);
        } catch (NoSuchFieldException e) {
            try {
                Field field = clazz.getDeclaredField(obfName);
                field.setAccessible(true);
                return new FieldWrapper(field);
            } catch (NoSuchFieldException e1) {
                if (!clazz.getSuperclass().isInstance(new Object())) {
                    return getFieldSuper(clazz, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Field not found!");
                }
            }
        }
    }

    public static FieldWrapper2 getField(Object o, String deobfName, String obfName) {
        try {
            Field f = o.getClass().getDeclaredField(deobfName);
            f.setAccessible(true);
            return new FieldWrapper2(f, o);
        } catch (NoSuchFieldException e) {
            try {
                Field f = o.getClass().getDeclaredField(obfName);
                f.setAccessible(true);
                return new FieldWrapper2(f, o);
            } catch (NoSuchFieldException e1) {
                if (!o.getClass().getSuperclass().isInstance(new Object())) {
                    return getFieldSuper(o, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Field not found!");
                }
            }
        }
    }

    private static FieldWrapper2 getFieldSuper(Object o, String deobfName, String obfName) {
        try {
            Field f = o.getClass().getSuperclass().getDeclaredField(deobfName);
            f.setAccessible(true);
            return new FieldWrapper2(f, o);
        } catch (NoSuchFieldException e) {
            try {
                Field f = o.getClass().getSuperclass().getDeclaredField(obfName);
                f.setAccessible(true);
                return new FieldWrapper2(f, o);
            } catch (NoSuchFieldException e1) {
                if (!o.getClass().getSuperclass().isInstance(new Object())) {
                    return getFieldSuper(o, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Field not found!");
                }
            }
        }
    }

    private static FieldWrapper getFieldSuper(Class<?> clazz, String deobfName, String obfName) {
        try {
            Field field = clazz.getSuperclass().getDeclaredField(deobfName);
            field.setAccessible(true);
            return new FieldWrapper(field);
        } catch (NoSuchFieldException e) {
            try {
                Field field = clazz.getSuperclass().getDeclaredField(obfName);
                field.setAccessible(true);
                return new FieldWrapper(field);
            } catch (NoSuchFieldException e1) {
                if (!clazz.getSuperclass().isInstance(new Object())) {
                    return getFieldSuper(clazz, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Field not found!");
                }
            }
        }
    }

    public static MethodWrapper2 getMethod(Object o, String deobfName, String obfName, Class<?>... params) {
        try {
            Method m = o.getClass().getDeclaredMethod(deobfName, params);
            m.setAccessible(true);
            return new MethodWrapper2(m, o);
        } catch (NoSuchMethodException e) {
            try {
                Method m = o.getClass().getDeclaredMethod(obfName, params);
                m.setAccessible(true);
                return new MethodWrapper2(m, o);
            } catch (NoSuchMethodException e1) {
                if (!o.getClass().getSuperclass().isInstance(new Object())) {
                    return getMethodSuper(o, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Method not found!");
                }
            }
        }
    }
    private static MethodWrapper2 getMethodSuper(Object o, String deobfName, String obfName, Class<?>... params) {
        try {
            Method m = o.getClass().getSuperclass().getDeclaredMethod(deobfName, params);
            m.setAccessible(true);
            return new MethodWrapper2(m, o);
        } catch (NoSuchMethodException e) {
            try {
                Method m = o.getClass().getSuperclass().getDeclaredMethod(obfName, params);
                m.setAccessible(true);
                return new MethodWrapper2(m, o);
            } catch (NoSuchMethodException e1) {
                if (!o.getClass().getSuperclass().isInstance(new Object())) {
                    return getMethodSuper(o, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Method not found!");
                }
            }
        }
    }
    private static MethodWrapper getMethodSuper(Class<?> clazz, String deobfName, String obfName, Class<?>... params) {
        try {
            Method m = clazz.getSuperclass().getDeclaredMethod(deobfName, params);
            m.setAccessible(true);
            return new MethodWrapper(m);
        } catch (NoSuchMethodException e) {
            try {
                Method m = clazz.getSuperclass().getDeclaredMethod(obfName, params);
                m.setAccessible(true);
                return new MethodWrapper(m);
            } catch (NoSuchMethodException e1) {
                if (!clazz.getSuperclass().isInstance(new Object())) {
                    return getMethodSuper(clazz, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Method not found!");
                }
            }
        }
    }
    public static MethodWrapper getMethod(Class<?> clazz, String deobfName, String obfName, Class<?>... params) {
        try {
            Method m = clazz.getDeclaredMethod(deobfName, params);
            m.setAccessible(true);
            return new MethodWrapper(m);
        } catch (NoSuchMethodException e) {
            try {
                Method m = clazz.getDeclaredMethod(obfName, params);
                m.setAccessible(true);
                return new MethodWrapper(m);
            } catch (NoSuchMethodException e1) {
                if (!clazz.getSuperclass().isInstance(new Object())) {
                    return getMethodSuper(clazz, deobfName, obfName);
                } else {
                    throw new IllegalStateException("Method not found!");
                }
            }
        }
    }

    public static Timer getTimer() {
        return (Timer) getField(Minecraft.getMinecraft(), "timer", "Y").get();
    }

    public static void setTimer(Timer t) {
        getField(Minecraft.getMinecraft(), "timer", "Y").set(t);
    }

    public static void resize(int width, int height) {
        getMethod(Minecraft.getMinecraft(), "resize", "a", int.class, int.class).invoke(width, height);
    }
}
