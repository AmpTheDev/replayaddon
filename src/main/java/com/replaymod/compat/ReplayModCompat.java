package com.replaymod.compat;

import cc.hyperium.event.InitializationEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.replaymod.compat.optifine.DisableFastRender;
import com.replaymod.compat.oranges17animations.HideInvisibleEntities;
import com.replaymod.core.utils.ReplayEventBus;

public class ReplayModCompat {

    @Subscribe
    public void init(InitializationEvent event) {
        EventBus bus = ReplayEventBus.INSTANCE;
        bus.register(new DisableFastRender());
        bus.register(new HideInvisibleEntities());
    }

}
