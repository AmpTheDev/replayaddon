package com.replaymod.compat.optifine;

import com.google.common.eventbus.Subscribe;
import com.replaymod.optifinedetector.OptifineDetector;
import com.replaymod.render.events.ReplayRenderEvent;
import net.minecraft.client.Minecraft;
public class DisableFastRender {

    private final Minecraft mc = Minecraft.getMinecraft();

    OptifineDetector od = new OptifineDetector();
    private boolean wasFastRender = false;

    @Subscribe
    public void onRenderBegin(ReplayRenderEvent.Pre event) {
        if (od.isOptifine()) return;

        try {
            wasFastRender = (boolean) OptifineReflection.gameSettings_ofFastRender.get(mc.gameSettings);
            OptifineReflection.gameSettings_ofFastRender.set(mc.gameSettings, false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onRenderEnd(ReplayRenderEvent.Post event){
        if (od.isOptifine()) return;

        try {
            OptifineReflection.gameSettings_ofFastRender.set(mc.gameSettings, wasFastRender);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
