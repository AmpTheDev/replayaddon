package com.replaymod.optifinedetector;

public class OptifineDetector {

    public boolean isOptifine() {
        try {
            Class.forName("optifine.json.JSONArray");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
