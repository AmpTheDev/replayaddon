package com.replaymod.recording;

import cc.hyperium.event.InitializationEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.replaymod.core.ReplayMod;
import com.replaymod.core.utils.ReplayEventBus;
import com.replaymod.recording.handler.ConnectionEventHandler;
import com.replaymod.recording.packet.PacketListener;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import net.minecraft.network.NetworkManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.input.Keyboard;

public class ReplayModRecording {
    public static final String MOD_ID = "replaymod-recording";

    public static ReplayModRecording instance;

    private ReplayMod core;

    private Logger logger;

    private ConnectionEventHandler connectionEventHandler;

    @Subscribe
    public void preInit(InitializationEvent event) {
        instance = this;
        logger = LogManager.getLogger("replaymod-recording");
        core = ReplayMod.instance;

        core.getSettingsRegistry().register(Setting.class);

        core.getKeyBindingRegistry().registerKeyBinding("replaymod.input.marker", Keyboard.KEY_M, new Runnable() {
            @Override
            public void run() {
                PacketListener packetListener = connectionEventHandler.getPacketListener();
                if (packetListener != null) {
                    packetListener.addMarker();
                    core.printInfoToChat("replaymod.chat.addedmarker");
                }
            }
        });
    }

    @Subscribe
    public void init(InitializationEvent event) {
        EventBus bus = ReplayEventBus.INSTANCE;
        bus.register(connectionEventHandler = new ConnectionEventHandler(logger, core));
    }

    @ChannelHandler.Sharable
    private static class RestrictionsChannelHandler extends ChannelDuplexHandler {}

    public void initiateRecording(NetworkManager networkManager) {
        connectionEventHandler.onConnectedToServerEvent(networkManager);
    }
}
