package com.replaymod.extras;

import cc.hyperium.event.InitializationEvent;
import cc.hyperium.event.PreInitializationEvent;
import com.google.common.eventbus.Subscribe;
import com.replaymod.core.ReplayMod;
import com.replaymod.extras.advancedscreenshots.AdvancedScreenshots;
import com.replaymod.extras.playeroverview.PlayerOverview;
import com.replaymod.extras.urischeme.UriSchemeExtra;
import com.replaymod.extras.youtube.YoutubeUpload;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReplayModExtras {
    public static final String MOD_ID = "replaymod-extras";
    public static ReplayModExtras instance;

    private static final List<Class<? extends Extra>> builtin = Arrays.asList(
            AdvancedScreenshots.class,
            PlayerOverview.class,
            UriSchemeExtra.class,
            YoutubeUpload.class,
            FullBrightness.class,
            HotkeyButtons.class,
            LocalizationExtra.class
    );

    private final Map<Class<? extends Extra>, Extra> instances = new HashMap<>();

    public static Logger LOGGER = LogManager.getLogger();

    @Subscribe
    public void preInit(PreInitializationEvent event) {
        instance = this;
    }

    @Subscribe
    public void init(InitializationEvent event) {
        ReplayMod.instance.getSettingsRegistry().register(Setting.class);
        for (Class<? extends Extra> cls : builtin) {
            try {
                Extra extra = cls.newInstance();
                extra.register(ReplayMod.instance);
                instances.put(cls, extra);
            } catch (Throwable t) {
                LOGGER.warn("Failed to load extra " + cls.getName() + ": ", t);
            }
        }
    }

    public <T extends Extra> Optional<T> get(Class<T> cls) {
        return Optional.ofNullable(instances.get(cls)).map(cls::cast);
    }
}
