package com.replaymod.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Configuration {
    private HashMap<String, Object> data = new HashMap<>();
    private static final Logger LOGGER = LogManager.getLogger("Replay Mod");
    private File file;
    private final Gson gson;
    public Configuration(File file) {
        this(file, true);
    }
    public Configuration(File file, boolean prettyPrinting) {
        this.file = file;
        file.mkdirs();
        this.gson = prettyPrinting ? new GsonBuilder().setPrettyPrinting().create() : new Gson();
    }
    public Object get(String name) {
        return data.get(name);
    }
    public Object get(String name, Object defaultValue) {
        if (!data.containsKey(name)) {
            data.put(name, defaultValue);
        }
        return get(name);
    }
    public Configuration set(String name, Object value) {
        data.put(name, value);
        return this;
    }
    public Configuration load() {
        if (!file.exists()) {
            try {
                if (!file.exists()) {
                    this.save();
                }
                this.data = gson.fromJson(new FileReader(file), HashMap.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this;
    }
    public Configuration save(){
        if (!file.exists()) {
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter writer;
                gson.toJson(data, writer = new FileWriter(file));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}
