package com.replaymod.online;

import com.replaymod.config.Configuration;
import com.replaymod.online.api.ApiClient;
import com.replaymod.online.api.AuthData;
import com.replaymod.online.api.replay.holders.AuthConfirmation;

/**
 * Auth data stored in a {@link Configuration}.
 */
public class ConfigurationAuthData implements AuthData {

    private final Configuration config;
    private String userName;
    private String authKey;

    public ConfigurationAuthData(Configuration config) {
        this.config = config;
    }

    /**
     * Loads the data from the configuration and checks for its validity.
     * If the data is invalid, it is removed from the config.
     * @param apiClient Api client used for validating the auth data
     */
    public void load(ApiClient apiClient) {
        String authKey = (String) config.get("authkey", (String) null);
        if (authKey != null) {
            AuthConfirmation result = apiClient.checkAuthkey(authKey);
            if (result != null) {
                this.authKey = authKey;
                this.userName = result.getUsername();
            } else {
                setData(null, null);
            }
        }
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getAuthKey() {
        return authKey;
    }

    @Override
    public void setData(String userName, String authKey) {
        this.userName = userName;
        this.authKey = authKey;

        if (authKey != null) {
            // Note: .get() actually creates the entry with the default value if it doesn't exist
            config.get("authkey", authKey);
        }
        config.save();
    }
}
