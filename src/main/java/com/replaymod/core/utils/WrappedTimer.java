package com.replaymod.core.utils;

import com.replaymod.hyperiumcompat.FieldWrapper;
import com.replaymod.hyperiumcompat.ReflectionHelper;
import net.minecraft.util.Timer;

public class WrappedTimer extends Timer {
    protected final Timer wrapped;

    public WrappedTimer(Timer wrapped) {
        super(0);
        this.wrapped = wrapped;
        copy(wrapped, this);
    }

    @Override
    public void updateTimer() {
        copy(this, wrapped);
        wrapped.updateTimer();
        copy(wrapped, this);
    }

    protected void copy(Timer from, Timer to) {
        FieldWrapper tps = ReflectionHelper.getField(Timer.class, "ticksPerSecond", "a");
        FieldWrapper lHRT = ReflectionHelper.getField(Timer.class, "lastHRTime", "f");
        FieldWrapper lSSC = ReflectionHelper.getField(Timer.class, "lastSyncSysClock", "g");
        FieldWrapper lSHRC = ReflectionHelper.getField(Timer.class, "lastSyncHRClock", "h");
        FieldWrapper c = ReflectionHelper.getField(Timer.class, "field_74285_i", "field_74285_i");
        FieldWrapper tSA = ReflectionHelper.getField(Timer.class, "timeSyncAdjustment", "j");
        tps.set(to, tps.get(from));
        lHRT.set(to, lHRT.get(from));
        lSSC.set(to, lSSC.get(from));
        lSHRC.set(to, lSHRC.get(from));
        c.set(to, c.get(from));
        tSA.set(to, tSA.get(from));
        tps.set(to, tps.get(from));
        to.elapsedTicks = from.elapsedTicks;
        to.renderPartialTicks = from.renderPartialTicks;
        to.timerSpeed = from.timerSpeed;
        to.elapsedPartialTicks = from.elapsedPartialTicks;
    }
}
