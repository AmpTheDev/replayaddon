package com.replaymod.core.utils;

import com.google.common.eventbus.EventBus;

public class ReplayEventBus {
  public static final ReplayEventBus R_INSTANCE = new ReplayEventBus();
  public static final EventBus INSTANCE = new EventBus();
  public static final EventBus PRIORITY_HIGH = new EventBus();

  public void onEvent(Object event) {
    // System.out.println("Event called: " + event.getClass().getName());
    PRIORITY_HIGH.post(event);
    INSTANCE.post(event);
  }
}
